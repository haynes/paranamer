package com.gitlab.haynes.paranamer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import com.gitlab.haynes.paranamer.DefaultParanamer;
import com.gitlab.haynes.paranamer.NullParanamer;
import com.gitlab.haynes.paranamer.ParameterNamesNotFoundException;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;

public class NullParanamerTestCase {

    @Test
    public void testNoNamesForMethod() throws Exception {
        NullParanamer paranamer = new NullParanamer();
        Method method = DefaultParanamer.class.getDeclaredMethod("getParameterTypeName", new Class[] {Class.class});
        String[] names = paranamer.lookupParameterNames(method);
        assertEquals(0, names.length);
    }

    @Test
    public void testNoNamesForMethodCanThrowReequiredException() throws Exception {
        NullParanamer paranamer = new NullParanamer();
        Method method = DefaultParanamer.class.getDeclaredMethod("getParameterTypeName", new Class[] {Class.class});
        try {
            paranamer.lookupParameterNames(method, true);
            fail("should have barfed");
        } catch (ParameterNamesNotFoundException e) {
            // expected
        }
    }

}
